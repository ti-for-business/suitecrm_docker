### SuiteCRM deployment

Links

1. [SuiteCRM](https://suitecrm.com/)
2. [Bitnami in docker hub](https://hub.docker.com/r/bitnami/suitecrm)
3. [Docker compose](https://raw.githubusercontent.com/bitnami/bitnami-docker-suitecrm/master/docker-compose.yml)

the default user and password from upstream is user/bitnami